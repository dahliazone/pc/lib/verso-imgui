#!/bin/bash

echo "verso-imgui: Fetching externals from repositories..."

if [ ! -d ../ocornut-imgui ]; then
        git clone https://gitlab.com/dahliazone/pc/extlib/ocornut-imgui.git ../ocornut-imgui
else
        pushd ../ocornut-imgui
        git pull --rebase
        popd
fi

if [ ! -d ../verso-3d ]; then
        git clone https://gitlab.com/dahliazone/pc/lib/verso-3d.git ../verso-3d
else
        pushd ../verso-3d
        git pull --rebase
        popd
fi

echo "verso-3d: Resursing into..."
pushd ../verso-3d
./fetch_parent_externals.sh
popd

echo "verso-imgui: All done"


#pragma once

#include <Verso/Display/IWindowOpengl.hpp>
#include <Verso/Time/FrameTimestamp.hpp>
#include <Verso/verso-imgui-common.hpp>

struct ImGuiContext;

namespace Verso {


class ImGuiHelper
{
private:
	bool created;
	ImGuiContext* imGuiContext;

public:
	VERSO_IMGUI_API ImGuiHelper();

	ImGuiHelper(const ImGuiHelper& original) = delete;

	VERSO_IMGUI_API ImGuiHelper(ImGuiHelper&& original);

	ImGuiHelper& operator =(const ImGuiHelper& original) = delete;

	VERSO_IMGUI_API ImGuiHelper& operator =(ImGuiHelper&& original);

	VERSO_IMGUI_API ~ImGuiHelper();

public:
	VERSO_IMGUI_API bool create(IWindowOpengl& window, bool useIniFiles);

	VERSO_IMGUI_API void destroy() VERSO_NOEXCEPT;

	VERSO_IMGUI_API ImGuiContext* getContext();

	VERSO_IMGUI_API bool isCreated();

	VERSO_IMGUI_API bool handleEvent(const IWindowOpengl& window, const FrameTimestamp& time, const Event& event);

	VERSO_IMGUI_API void newFrame(IWindowOpengl& window, const FrameTimestamp& time, bool useRenderResolution);

	VERSO_IMGUI_API void render(const IWindowOpengl& window, const Recti& viewportRect);

	VERSO_IMGUI_API ImGuiContext* getImGuiContext();
};


} // End namespace Verso



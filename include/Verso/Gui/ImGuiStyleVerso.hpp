#pragma once

#include <Verso/System/UString.hpp>
#include <Verso/verso-imgui-common.hpp>
#include <Verso/System/JsonPath.hpp>

#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui.h>
#include <imgui_internal.h>
#include <map>

namespace Verso {


class ImGuiStyleVerso
{
public:
	static std::map<UString, ImGuiCol_> colorsNames;

private:
	UString name;
	UString defaultTheme;
	bool calculatedColors;
	ImGuiStyle style;

public:
	VERSO_IMGUI_API ImGuiStyleVerso();

	VERSO_IMGUI_API ImGuiStyleVerso(const ImGuiStyleVerso& original);

	VERSO_IMGUI_API ImGuiStyleVerso(ImGuiStyleVerso&& original) noexcept;

	VERSO_IMGUI_API ImGuiStyleVerso& operator =(const ImGuiStyleVerso& original);

	VERSO_IMGUI_API ImGuiStyleVerso& operator =(ImGuiStyleVerso&& original) noexcept;

	VERSO_IMGUI_API ~ImGuiStyleVerso();

public:
	VERSO_IMGUI_API void loadFromFile(const UString fileNameJson);

	VERSO_IMGUI_API void importJson(const JsonPath& parent, const UString& name, bool required);

	VERSO_IMGUI_API JSONValue* exportJson() const;

	VERSO_IMGUI_API void applyStyleToImGui();

public: // toString
	VERSO_IMGUI_API UString toString() const;

	VERSO_IMGUI_API UString toStringDebug() const;

	friend std::ostream& operator <<(std::ostream& ost, const ImGuiStyleVerso& right)
	{
		return ost << right.toString();
	}
};


} // End namespace ImGui

// dear imgui: Platform Backend for Verso

#pragma once
#include "imgui.h"      // IMGUI_IMPL_API
#ifndef IMGUI_DISABLE

#include <Verso/verso-imgui-common.hpp>

namespace Verso {

class IWindowOpengl;
//struct _SDL_GameController; // TODO: Add verso implementation of gamepad support
struct Event;
class FrameTimestamp;


IMGUI_IMPL_API bool     ImGui_ImplVerso_InitForOpenGL(IWindowOpengl& window, void* sdl_gl_context);
IMGUI_IMPL_API void     ImGui_ImplVerso_Shutdown();
IMGUI_IMPL_API void     ImGui_ImplVerso_NewFrame(IWindowOpengl& window, const FrameTimestamp& time, bool useRenderResolution);
IMGUI_IMPL_API bool     ImGui_ImplVerso_ProcessEvent(const Event& event);


// TODO: Add verso implementation of gamepad support
// Gamepad selection automatically starts in AutoFirst mode, picking first available SDL_Gamepad. You may override this.
// When using manual mode, caller is responsible for opening/closing gamepad.
//enum ImGui_ImplSDL2_GamepadMode { ImGui_ImplSDL2_GamepadMode_AutoFirst, ImGui_ImplSDL2_GamepadMode_AutoAll, ImGui_ImplSDL2_GamepadMode_Manual };
//IMGUI_IMPL_API void     ImGui_ImplSDL2_SetGamepadMode(ImGui_ImplSDL2_GamepadMode mode, struct _SDL_GameController** manual_gamepads_array = NULL, int manual_gamepads_count = -1);


} // End namespace Verso


#endif // #ifndef IMGUI_DISABLE

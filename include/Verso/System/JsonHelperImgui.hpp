#pragma once

#include <Verso/verso-imgui-common.hpp>
#include <Verso/System/JsonHelper3d.hpp>

#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui.h>

namespace Verso {

namespace JsonHelper {


VERSO_IMGUI_API ImVec2 readImVec2(const JsonPath& parent, const UString& name, bool required = false, const ImVec2& defaultValue = ImVec2(0, 0));
VERSO_IMGUI_API ImVec4 readImVec4(const JsonPath& parent, const UString& name, bool required = false, const ImVec4& defaultValue = ImVec4(0, 0, 0, 0));
VERSO_IMGUI_API ImGuiDir readImGuiDir(const JsonPath& parent, const UString& name, bool required = false, const ImGuiDir& defaultValue = ImGuiDir(ImGuiDir_None));

inline UString imVec2ToString(const ImVec2& v)
{
    UString str("[ ");
    str.append2(v.x);
    str.append(", ");
    str.append2(v.y);
    str.append(" ]");
    return str;
}


inline UString imVec4ToString(const ImVec4& v)
{
    UString str("[ ");
    str.append2(v.x);
    str.append(", ");
    str.append2(v.y);
    str.append(", ");
    str.append2(v.z);
    str.append(", ");
    str.append2(v.w);
    str.append(" ]");
    return str;
}


inline ImGuiDir stringToImGuiDir(const UString& str)
{
    if (str.equals("None")) {
        return ImGuiDir_None;
    }
    else if (str.equals("Left")) {
        return ImGuiDir_Left;
    }
    else if (str.equals("Right")) {
        return ImGuiDir_Right;
    }
    else if (str.equals("Up")) {
        return ImGuiDir_Up;
    }
    else if (str.equals("Down")) {
        return ImGuiDir_Down;
    }
    else {
        return ImGuiDir_COUNT;
    }
}


inline UString imGuiDirToString(const ImGuiDir& dir)
{
    if (dir == ImGuiDir_None) {
        return "None";
    }
    else if (dir == ImGuiDir_Left) {
        return "Left";
    }
    else if (dir == ImGuiDir_Right) {
        return "Right";
    }
    else if (dir == ImGuiDir_Up) {
        return "Up";
    }
    else if (dir == ImGuiDir_Down) {
        return "Down";
    }
    else {
        return "";
    }
}


} // End namespace JsonHelper


} // End namespace Verso

#pragma once

#if defined(WIN32) || defined _WIN32 || defined __CYGWIN__
#	if defined(VERSO_IMGUI_BUILD_DYNAMIC)
#		ifdef __GNUC__
#			define VERSO_IMGUI_API __attribute__ ((dllexport))
#		else
#			define VERSO_IMGUI_API __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
#			define VERSO_IMGUI_EXPIMP
#		endif
#	else
#		ifdef __GNUC__
#			define VERSO_IMGUI_API __attribute__ ((dllimport))
#		else
#			define VERSO_IMGUI_API __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
#			define VERSO_IMGUI_EXPIMP extern
#		endif
#	endif
#	define VERSO_IMGUI_PRIVATE
#else
#	if __GNUC__ >= 4
#		define VERSO_IMGUI_API __attribute__ ((visibility ("default")))
#		define VERSO_IMGUI_PRIVATE  __attribute__ ((visibility ("hidden")))
#	else
#		define VERSO_IMGUI_API
#		define VERSO_IMGUI_PRIVATE
#	endif
#endif



#include <Verso/Gui/ImGuiHelper.hpp>
#include <Verso/Gui/imgui/imgui_impl_verso.hpp>
#include <backends/imgui_impl_opengl3.h>

#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui.h>
//#include <imgui_internal.h>

namespace Verso {


///////////////////////////////////////////////////////////////////////////////////////////
// ImGuiHelper class implementation
///////////////////////////////////////////////////////////////////////////////////////////

ImGuiHelper::ImGuiHelper() :
	created(false),
	imGuiContext(nullptr)
{
}


ImGuiHelper::ImGuiHelper(ImGuiHelper&& original) :
	created(std::move(original.created)),
	imGuiContext(std::move(original.imGuiContext))
{
}


ImGuiHelper& ImGuiHelper::operator =(ImGuiHelper&& original)
{
	if (this != &original) {
		created = std::move(original.created);
		imGuiContext = std::move(original.imGuiContext);

		// clear original
		original.created = false;
		original.imGuiContext = nullptr;
	}
	return *this;
}


ImGuiHelper::~ImGuiHelper()
{
	ImGuiHelper::destroy();
}


bool ImGuiHelper::create(IWindowOpengl& window, bool useIniFiles) {
	if (isCreated() == true) {
		return false;
	}

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	imGuiContext = ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

	// Setup Dear ImGui style \TODO: not here!
	ImGui::StyleColorsDark();
	//ImGui::StyleColorsLight();

	// Setup Platform/Renderer backends
	void* sdl_gl_context = nullptr; // \TODO: MULTI VIEWPORT
	ImGui_ImplVerso_InitForOpenGL(window, sdl_gl_context);
	ImGui_ImplOpenGL3_Init(window.getSettings().glslVersionString.c_str());

	// Load Fonts
	// - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
	// - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
	// - If the file cannot be loaded, the function will return a nullptr. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
	// - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
	// - Use '#define IMGUI_ENABLE_FREETYPE' in your imconfig file to use Freetype for higher quality font rendering.
	// - Read 'docs/FONTS.md' for more instructions and details.
	// - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
	// - Our Emscripten build process allows embedding fonts to be accessible at runtime from the "fonts/" folder. See Makefile.emscripten for details.
	//io.Fonts->AddFontDefault();
	//io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\segoeui.ttf", 18.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
	//ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, nullptr, io.Fonts->GetGlyphRangesJapanese());
	//IM_ASSERT(font != nullptr);

	if (!useIniFiles) {
		io.IniFilename = nullptr;
	}

	created = true;
	return true;
}


void ImGuiHelper::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}
	ImGui::SetCurrentContext(imGuiContext);

	// Cleanup
	ImGuiIO& io = ImGui::GetIO();
	io.Fonts->Clear();

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplVerso_Shutdown();
	ImGui::DestroyContext();

	created = false;
}


ImGuiContext* ImGuiHelper::getContext()
{
	return imGuiContext;
}


bool ImGuiHelper::isCreated()
{
	return created;
}


bool ImGuiHelper::handleEvent(const IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
	(void)window; (void)time;
	ImGui::SetCurrentContext(imGuiContext);
	// TODO: io.WantCaptureMouse & io.WantCaptureKeyboard
	// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
	// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application, or clear/overwrite your copy of the mouse data.
	// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application, or clear/overwrite your copy of the keyboard data.
	// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
	return ImGui_ImplVerso_ProcessEvent(event);
}


void ImGuiHelper::newFrame(IWindowOpengl& window, const FrameTimestamp& time, bool useRenderResolution)
{
	ImGui::SetCurrentContext(imGuiContext);
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplVerso_NewFrame(window, time, useRenderResolution);
	ImGui::NewFrame();
}


void ImGuiHelper::render(const IWindowOpengl& window, const Recti& viewportRect)
{
	(void)window; (void)viewportRect;

	ImGui::SetCurrentContext(imGuiContext);
	ImGui::Render();
    ImGuiIO& io = ImGui::GetIO();
	glViewport(0, 0, static_cast<int>(io.DisplaySize.x), static_cast<int>(io.DisplaySize.y));
	GL_CHECK_FOR_ERRORS("", "");
	// TODO: window.applyDrawableViewport(); or window.applyRenderViewport();

	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	GL_CHECK_FOR_ERRORS("", "");
}


} // End namespace Verso


#include <Verso/Gui/ImGuiStyleVerso.hpp>
#include <Verso/System/JsonHelperImgui.hpp>

namespace Verso {


std::map<UString, ImGuiCol_> ImGuiStyleVerso::colorsNames = std::map<UString, ImGuiCol_>();


ImGuiStyleVerso::ImGuiStyleVerso() :
	name(),
	defaultTheme(),
	calculatedColors(true),
	style()
{
	if (colorsNames.empty()) {
		colorsNames["Text"] = ImGuiCol_Text;
		colorsNames["TextDisabled"] = ImGuiCol_TextDisabled;
		colorsNames["WindowBg"] = ImGuiCol_WindowBg;                            // Background of normal windows
		colorsNames["ChildBg"] = ImGuiCol_ChildBg;                              // Background of child windows
		colorsNames["PopupBg"] = ImGuiCol_PopupBg;                              // Background of popups, menus, tooltips windows
		colorsNames["Border"] = ImGuiCol_Border;
		colorsNames["BorderShadow"] = ImGuiCol_BorderShadow;
		colorsNames["FrameBg"] = ImGuiCol_FrameBg;                              // Background of checkbox, radio button, plot, slider, text input
		colorsNames["FrameBgHovered"] = ImGuiCol_FrameBgHovered;
		colorsNames["FrameBgActive"] = ImGuiCol_FrameBgActive;
		colorsNames["TitleBg"] = ImGuiCol_TitleBg;                              // Title bar
		colorsNames["TitleBgActive"] = ImGuiCol_TitleBgActive;                  // Title bar when focused
		colorsNames["TitleBgCollapsed"] = ImGuiCol_TitleBgCollapsed;            // Title bar when collapsed
		colorsNames["MenuBarBg"] = ImGuiCol_MenuBarBg;
		colorsNames["ScrollbarBg"] = ImGuiCol_ScrollbarBg;
		colorsNames["ScrollbarGrab"] = ImGuiCol_ScrollbarGrab;
		colorsNames["ScrollbarGrabHovered"] = ImGuiCol_ScrollbarGrabHovered;
		colorsNames["ScrollbarGrabActive"] = ImGuiCol_ScrollbarGrabActive;
		colorsNames["CheckMark"] = ImGuiCol_CheckMark;                          // Checkbox tick and RadioButton circle
		colorsNames["SliderGrab"] = ImGuiCol_SliderGrab;
		colorsNames["SliderGrabActive"] = ImGuiCol_SliderGrabActive;
		colorsNames["Button"] = ImGuiCol_Button;
		colorsNames["ButtonHovered"] = ImGuiCol_ButtonHovered;
		colorsNames["ButtonActive"] = ImGuiCol_ButtonActive;
		colorsNames["Header"] = ImGuiCol_Header;                                // Header* colors are used for CollapsingHeader, TreeNode, Selectable, MenuItem
		colorsNames["HeaderHovered"] = ImGuiCol_HeaderHovered;
		colorsNames["HeaderActive"] = ImGuiCol_HeaderActive;
		colorsNames["Separator"] = ImGuiCol_Separator;
		colorsNames["SeparatorHovered"] = ImGuiCol_SeparatorHovered;
		colorsNames["SeparatorActive"] = ImGuiCol_SeparatorActive;
		colorsNames["ResizeGrip"] = ImGuiCol_ResizeGrip;                        // Resize grip in lower-right and lower-left corners of windows.
		colorsNames["ResizeGripHovered"] = ImGuiCol_ResizeGripHovered;
		colorsNames["ResizeGripActive"] = ImGuiCol_ResizeGripActive;
		colorsNames["Tab"] = ImGuiCol_Tab;                                      // TabItem in a TabBar
		colorsNames["TabHovered"] = ImGuiCol_TabHovered;
		colorsNames["TabActive"] = ImGuiCol_TabActive;
		colorsNames["TabUnfocused"] = ImGuiCol_TabUnfocused;
		colorsNames["TabUnfocusedActive"] = ImGuiCol_TabUnfocusedActive;
		colorsNames["PlotLines"] = ImGuiCol_PlotLines;
		colorsNames["PlotLinesHovered"] = ImGuiCol_PlotLinesHovered;
		colorsNames["PlotHistogram"] = ImGuiCol_PlotHistogram;
		colorsNames["PlotHistogramHovered"] = ImGuiCol_PlotHistogramHovered;
		colorsNames["TableHeaderBg"] = ImGuiCol_TableHeaderBg;                  // Table header background
		colorsNames["ImGuiCol_TableBorderStrong"] = ImGuiCol_TableBorderStrong; // Table outer and header borders (prefer using Alpha=1.0 here)
		colorsNames["ImGuiCol_TableBorderLight"] = ImGuiCol_TableBorderLight;   // Table inner borders (prefer using Alpha=1.0 here)
		colorsNames["ImGuiCol_TableRowBg"] = ImGuiCol_TableRowBg;               // Table row background (even rows)
		colorsNames["ImGuiCol_TableRowBgAlt"] = ImGuiCol_TableRowBgAlt;         // Table row background (odd rows)
		colorsNames["TextSelectedBg"] = ImGuiCol_TextSelectedBg;
		colorsNames["DragDropTarget"] = ImGuiCol_DragDropTarget;                // Rectangle highlighting a drop target
		colorsNames["NavHighlight"] = ImGuiCol_NavHighlight;                   // Gamepad/keyboard: current highlighted item
		colorsNames["NavWindowingHighlight"] = ImGuiCol_NavWindowingHighlight; // Highlight window when using CTRL+TAB
		colorsNames["NavWindowingDimBg"] = ImGuiCol_NavWindowingDimBg;         // Darken/colorize entire screen behind the CTRL+TAB window list, when active
		colorsNames["ModalWindowDimBg"] = ImGuiCol_ModalWindowDimBg;           // Darken/colorize entire screen behind a modal window, when one is active
	}
}


ImGuiStyleVerso::ImGuiStyleVerso(const ImGuiStyleVerso& original) :
	name(original.name),
	defaultTheme(original.defaultTheme),
	calculatedColors(original.calculatedColors),
	style(original.style)
{
}


ImGuiStyleVerso::ImGuiStyleVerso(ImGuiStyleVerso&& original) noexcept :
	name(std::move(original.name)),
	defaultTheme(std::move(original.defaultTheme)),
	calculatedColors(std::move(original.calculatedColors)),
	style(std::move(original.style))
{
}


ImGuiStyleVerso& ImGuiStyleVerso::operator =(const ImGuiStyleVerso& original)
{
	if (this == &original) {
		return *this;
	}

	name = original.name;
	defaultTheme = original.defaultTheme;
	calculatedColors = original.calculatedColors;
	style = original.style;
	return *this;
}


ImGuiStyleVerso& ImGuiStyleVerso::operator =(ImGuiStyleVerso&& original) noexcept
{
	if (this == &original) {
		return *this;
	}

	name = std::move(original.name);
	defaultTheme = std::move(original.defaultTheme);
	calculatedColors = std::move(original.calculatedColors);
	style = std::move(original.style);
	return *this;
}


ImGuiStyleVerso::~ImGuiStyleVerso()
= default;


void ImGuiStyleVerso::loadFromFile(const UString fileNameJson)
{
	JsonPath rootPath = JsonHelper::loadAndParse(fileNameJson);

	importJson(rootPath, "", false);

	JsonHelper::free(rootPath);
}


void ImGuiStyleVerso::importJson(const JsonPath& parent, const UString& name, bool required)
{
	JsonPath stylePath = parent.getAttribute(name, required);
	this->name = stylePath.readString("name", false, "");

	this->defaultTheme = stylePath.readString("defaultTheme", false, "Dark");
	if (!this->defaultTheme.equals("Dark") && !this->defaultTheme.equals("Classic") && this->defaultTheme.equals("Light")) {
		UString error("Unknown value (" + this->defaultTheme + ") in field \"" + stylePath.currentPath + ".defaultTheme" + R"("! Must be one of: "Dark", "Classic", "Light".)");
		VERSO_ILLEGALFORMAT("verso-imgui", error.c_str(), parent.fileName.c_str());
	}

	this->calculatedColors = stylePath.readBool("calculatedColors", false, true);

	ImGuiStyle defaults;
	if (this->defaultTheme.equals("Dark")) {
		ImGui::StyleColorsDark(&defaults);
	}
	else if (this->defaultTheme.equals("Classic")) {
		ImGui::StyleColorsClassic(&defaults);
	}
	else if (this->defaultTheme.equals("Light")) {
		ImGui::StyleColorsLight(&defaults);
	}

	this->style.Alpha = stylePath.readNumberf("Alpha", false, defaults.Alpha);                                                            // Global alpha applies to everything in Dear ImGui.
	this->style.WindowPadding = JsonHelper::readImVec2(stylePath, "WindowPadding", false, defaults.WindowPadding);                                     // Padding within a window.
	this->style.WindowRounding = stylePath.readNumberf("WindowRounding", false, defaults.WindowRounding);                                 // Radius of window corners rounding. Set to 0.0f to have rectangular windows.
	this->style.WindowBorderSize = stylePath.readNumberf("WindowBorderSize", false, defaults.WindowBorderSize);                           // Thickness of border around windows. Generally set to 0.0f or 1.0f. (Other values are not well tested and more CPU/GPU costly).
	this->style.WindowMinSize = JsonHelper::readImVec2(stylePath, "WindowMinSize", false, defaults.WindowMinSize);                                     // Minimum window size. This is a global setting. If you want to constraint individual windows, use SetNextWindowSizeConstraints().
	this->style.WindowTitleAlign = JsonHelper::readImVec2(stylePath, "WindowTitleAlign", false, defaults.WindowTitleAlign);                            // Alignment for title bar text. Defaults to (0.0f,0.5f) for left-aligned,vertically centered.
	this->style.WindowMenuButtonPosition = JsonHelper::readImGuiDir(stylePath, "WindowMenuButtonPosition", false, defaults.WindowMenuButtonPosition);  // Side of the collapsing/docking button in the title bar (None/Left/Right). Defaults to ImGuiDir_Left.
	this->style.ChildRounding = stylePath.readNumberf("ChildRounding", false, defaults.ChildRounding);                                    // Radius of child window corners rounding. Set to 0.0f to have rectangular windows.
	this->style.ChildBorderSize = stylePath.readNumberf("ChildBorderSize", false, defaults.ChildBorderSize);                              // Thickness of border around child windows. Generally set to 0.0f or 1.0f. (Other values are not well tested and more CPU/GPU costly).
	this->style.PopupRounding = stylePath.readNumberf("PopupRounding", false, defaults.PopupRounding);                                    // Radius of popup window corners rounding. (Note that tooltip windows use WindowRounding)
	this->style.PopupBorderSize = stylePath.readNumberf("PopupBorderSize", false, defaults.PopupBorderSize);                              // Thickness of border around popup/tooltip windows. Generally set to 0.0f or 1.0f. (Other values are not well tested and more CPU/GPU costly).
	this->style.FramePadding = JsonHelper::readImVec2(stylePath, "FramePadding", false, defaults.FramePadding);                                        // Padding within a framed rectangle (used by most widgets).
	this->style.FrameRounding = stylePath.readNumberf("FrameRounding", false, defaults.FrameRounding);                                    // Radius of frame corners rounding. Set to 0.0f to have rectangular frame (used by most widgets).
	this->style.FrameBorderSize = stylePath.readNumberf("FrameBorderSize", false, defaults.FrameBorderSize);                              // Thickness of border around frames. Generally set to 0.0f or 1.0f. (Other values are not well tested and more CPU/GPU costly).
	this->style.ItemSpacing = JsonHelper::readImVec2(stylePath, "ItemSpacing", false, defaults.ItemSpacing);                                           // Horizontal and vertical spacing between widgets/lines.
	this->style.ItemInnerSpacing = JsonHelper::readImVec2(stylePath, "ItemInnerSpacing", false, defaults.ItemInnerSpacing);                            // Horizontal and vertical spacing between within elements of a composed widget (e.g. a slider and its label).
	this->style.TouchExtraPadding = JsonHelper::readImVec2(stylePath, "TouchExtraPadding", false, defaults.TouchExtraPadding);                         // Expand reactive bounding box for touch-based system where touch position is not accurate enough. Unfortunately we don't sort widgets so priority on overlap will always be given to the first widget. So don't grow this too much!
	this->style.IndentSpacing = stylePath.readNumberf("IndentSpacing", false, defaults.IndentSpacing);                                    // Horizontal indentation when e.g. entering a tree node. Generally == (FontSize + FramePadding.x*2).
	this->style.ColumnsMinSpacing = stylePath.readNumberf("ColumnsMinSpacing", false, defaults.ColumnsMinSpacing);                        // Minimum horizontal spacing between two columns. Preferably > (FramePadding.x + 1).
	this->style.ScrollbarSize = stylePath.readNumberf("ScrollbarSize", false, defaults.ScrollbarSize);                                    // Width of the vertical scrollbar, Height of the horizontal scrollbar.
	this->style.ScrollbarRounding = stylePath.readNumberf("ScrollbarRounding", false, defaults.ScrollbarRounding);                        // Radius of grab corners for scrollbar.
	this->style.GrabMinSize = stylePath.readNumberf("GrabMinSize", false, defaults.GrabMinSize);                                          // Minimum width/height of a grab box for slider/scrollbar.
	this->style.GrabRounding = stylePath.readNumberf("GrabRounding", false, defaults.GrabRounding);                                       // Radius of grabs corners rounding. Set to 0.0f to have rectangular slider grabs.
	this->style.TabRounding = stylePath.readNumberf("TabRounding", false, defaults.TabRounding);                                          // Radius of upper corners of a tab. Set to 0.0f to have rectangular tabs.
	this->style.TabBorderSize = stylePath.readNumberf("TabBorderSize", false, defaults.TabBorderSize);                                    // Thickness of border around tabs.
	this->style.ColorButtonPosition = JsonHelper::readImGuiDir(stylePath, "ColorButtonPosition", false, defaults.ColorButtonPosition);                 // Side of the color button in the ColorEdit4 widget (left/right). Defaults to ImGuiDir_Right.
	this->style.ButtonTextAlign = JsonHelper::readImVec2(stylePath, "ButtonTextAlign", false, defaults.ButtonTextAlign);                               // Alignment of button text when button is larger than text. Defaults to (0.5f, 0.5f) (centered).
	this->style.SelectableTextAlign = JsonHelper::readImVec2(stylePath, "SelectableTextAlign", false, defaults.SelectableTextAlign);                   // Alignment of selectable text when selectable is larger than text. Defaults to (0.0f, 0.0f) (top-left aligned).
	this->style.DisplayWindowPadding = JsonHelper::readImVec2(stylePath, "DisplayWindowPadding", false, defaults.DisplayWindowPadding);                // Window position are clamped to be visible within the display area or monitors by at least this amount. Only applies to regular windows.
	this->style.DisplaySafeAreaPadding = JsonHelper::readImVec2(stylePath, "DisplaySafeAreaPadding", false, defaults.DisplaySafeAreaPadding);          // If you cannot see the edges of your screen (e.g. on a TV) increase the safe area padding. Apply to popups/tooltips as well regular windows. NB: Prefer configuring your TV sets correctly!
	this->style.MouseCursorScale = stylePath.readNumberf("MouseCursorScale", false, defaults.MouseCursorScale);                           // Scale software rendered mouse cursor (when io.MouseDrawCursor is enabled). May be removed later.
	this->style.AntiAliasedLines = stylePath.readBool("AntiAliasedLines", false, defaults.AntiAliasedLines);                    // Enable anti-aliasing on lines/borders. Disable if you are really tight on CPU/GPU.
	this->style.AntiAliasedFill = stylePath.readBool("AntiAliasedFill", false, defaults.AntiAliasedFill);                       // Enable anti-aliasing on filled shapes (rounded rectangles, circles, etc.)
	this->style.CurveTessellationTol = stylePath.readNumberf("CurveTessellationTol", false, defaults.CurveTessellationTol);               // Tessellation tolerance when using PathBezierCurveTo() without a specific number of segments. Decrease for highly tessellated curves (higher quality, more polygons), increase to reduce quality.

	JsonPath colorsPath = stylePath.getAttribute("colors", false);
	for (auto& [name, colorEnum] : colorsNames) {
		this->style.Colors[colorEnum] = JsonHelper::readImVec4(colorsPath, name, false, defaults.Colors[colorEnum]);
	}

	if (this->calculatedColors) {
		if (this->defaultTheme.equals("Dark")) {
			this->style.Colors[ImGuiCol_Separator]              = this->style.Colors[ImGuiCol_Border];
			this->style.Colors[ImGuiCol_Tab]                    = ImLerp(this->style.Colors[ImGuiCol_Header],       this->style.Colors[ImGuiCol_TitleBgActive], 0.80f);
			this->style.Colors[ImGuiCol_TabHovered]             = this->style.Colors[ImGuiCol_HeaderHovered];
			this->style.Colors[ImGuiCol_TabActive]              = ImLerp(this->style.Colors[ImGuiCol_HeaderActive], this->style.Colors[ImGuiCol_TitleBgActive], 0.60f);
			this->style.Colors[ImGuiCol_TabUnfocused]           = ImLerp(this->style.Colors[ImGuiCol_Tab],          this->style.Colors[ImGuiCol_TitleBg], 0.80f);
			this->style.Colors[ImGuiCol_TabUnfocusedActive]     = ImLerp(this->style.Colors[ImGuiCol_TabActive],    this->style.Colors[ImGuiCol_TitleBg], 0.40f);
		}
		else if (this->defaultTheme.equals("Classic")) {
			this->style.Colors[ImGuiCol_Tab]                    = ImLerp(this->style.Colors[ImGuiCol_Header],       this->style.Colors[ImGuiCol_TitleBgActive], 0.80f);
			this->style.Colors[ImGuiCol_TabHovered]             = this->style.Colors[ImGuiCol_HeaderHovered];
			this->style.Colors[ImGuiCol_TabActive]              = ImLerp(this->style.Colors[ImGuiCol_HeaderActive], this->style.Colors[ImGuiCol_TitleBgActive], 0.60f);
			this->style.Colors[ImGuiCol_TabUnfocused]           = ImLerp(this->style.Colors[ImGuiCol_Tab],          this->style.Colors[ImGuiCol_TitleBg], 0.80f);
			this->style.Colors[ImGuiCol_TabUnfocusedActive]     = ImLerp(this->style.Colors[ImGuiCol_TabActive],    this->style.Colors[ImGuiCol_TitleBg], 0.40f);
			this->style.Colors[ImGuiCol_NavHighlight]           = this->style.Colors[ImGuiCol_HeaderHovered];
		}
		else if (this->defaultTheme.equals("Light")) {
			this->style.Colors[ImGuiCol_Tab]                    = ImLerp(this->style.Colors[ImGuiCol_Header],       this->style.Colors[ImGuiCol_TitleBgActive], 0.90f);
			this->style.Colors[ImGuiCol_TabHovered]             = this->style.Colors[ImGuiCol_HeaderHovered];
			this->style.Colors[ImGuiCol_TabActive]              = ImLerp(this->style.Colors[ImGuiCol_HeaderActive], this->style.Colors[ImGuiCol_TitleBgActive], 0.60f);
			this->style.Colors[ImGuiCol_TabUnfocused]           = ImLerp(this->style.Colors[ImGuiCol_Tab],          this->style.Colors[ImGuiCol_TitleBg], 0.80f);
			this->style.Colors[ImGuiCol_TabUnfocusedActive]     = ImLerp(this->style.Colors[ImGuiCol_TabActive],    this->style.Colors[ImGuiCol_TitleBg], 0.40f);
			this->style.Colors[ImGuiCol_NavHighlight]           = this->style.Colors[ImGuiCol_HeaderHovered];
		}
	}
}


JSONValue* ImGuiStyleVerso::exportJson() const
{
	VERSO_FAIL("verso-3d"); // \TODO: ImGuiStyleVerso::exportJson(): NOT IMPLEMENTED!
	return nullptr;
}


void ImGuiStyleVerso::applyStyleToImGui()
{
	ImGuiStyle& style = ImGui::GetStyle();

	style.Alpha = this->style.Alpha;
	style.WindowPadding = this->style.WindowPadding;
	style.WindowRounding = this->style.WindowRounding;
	style.WindowBorderSize = this->style.WindowBorderSize;
	style.WindowMinSize = this->style.WindowMinSize;
	style.WindowTitleAlign = this->style.WindowTitleAlign;
	style.WindowMenuButtonPosition = this->style.WindowMenuButtonPosition;
	style.ChildRounding = this->style.ChildRounding;
	style.ChildBorderSize = this->style.ChildBorderSize;
	style.PopupRounding = this->style.PopupRounding;
	style.PopupBorderSize = this->style.PopupBorderSize;
	style.FramePadding = this->style.FramePadding;
	style.FrameRounding = this->style.FrameRounding;
	style.FrameBorderSize = this->style.FrameBorderSize;
	style.ItemSpacing = this->style.ItemSpacing;
	style.ItemInnerSpacing = this->style.ItemInnerSpacing;
	style.TouchExtraPadding = this->style.TouchExtraPadding;
	style.IndentSpacing = this->style.IndentSpacing;
	style.ColumnsMinSpacing = this->style.ColumnsMinSpacing;
	style.ScrollbarSize = this->style.ScrollbarSize;
	style.ScrollbarRounding = this->style.ScrollbarRounding;
	style.GrabMinSize = this->style.GrabMinSize;
	style.GrabRounding = this->style.GrabRounding;
	style.TabRounding = this->style.TabRounding;
	style.TabBorderSize = this->style.TabBorderSize;
	style.ColorButtonPosition = this->style.ColorButtonPosition;
	style.ButtonTextAlign = this->style.ButtonTextAlign;
	style.SelectableTextAlign = this->style.SelectableTextAlign;
	style.DisplayWindowPadding = this->style.DisplayWindowPadding;
	style.DisplaySafeAreaPadding = this->style.DisplaySafeAreaPadding;
	style.MouseCursorScale = this->style.MouseCursorScale;
	style.AntiAliasedLines = this->style.AntiAliasedLines;
	style.AntiAliasedFill = this->style.AntiAliasedFill;
	style.CurveTessellationTol = this->style.CurveTessellationTol;

	for (int i=0; i<ImGuiCol_COUNT; i++) {
		style.Colors[i] = this->style.Colors[i];
	}
}


// toString
UString ImGuiStyleVerso::toString() const
{
	UString str("name=\"");
	str += name;
	str.append("\", defaultTheme=\"");
	str += defaultTheme;
	str.append("\", calculatedColors=");
	if (calculatedColors) {
		str.append("true");
	}
	else {
		str.append("false");
	}

	str.append(", style={\n");

	str.append("    Alpha=");
	str.append2(this->style.Alpha);
	str.append(",\n");

	str.append("    WindowPadding=");
	str += JsonHelper::imVec2ToString(this->style.WindowPadding);
	str.append(",\n");

	str.append("    WindowRounding=");
	str.append2(this->style.WindowRounding);
	str.append(",\n");

	str.append("    WindowBorderSize=");
	str.append2(this->style.WindowBorderSize);
	str.append(",\n");

	str.append("    WindowMinSize=");
	str += JsonHelper::imVec2ToString(this->style.WindowMinSize);
	str.append(",\n");

	str.append("    WindowTitleAlign=");
	str += JsonHelper::imVec2ToString(this->style.WindowTitleAlign);
	str.append(",\n");

	str.append("    WindowMenuButtonPosition=");
	str += JsonHelper::imGuiDirToString(this->style.WindowMenuButtonPosition);
	str.append(",\n");

	str.append("    ChildRounding=");
	str.append2(this->style.ChildRounding);
	str.append(",\n");

	str.append("    ChildBorderSize=");
	str.append2(this->style.ChildBorderSize);
	str.append(",\n");

	str.append("    PopupRounding=");
	str.append2(this->style.PopupRounding);
	str.append(",\n");

	str.append("    PopupBorderSize=");
	str.append2(this->style.PopupBorderSize);
	str.append(",\n");

	str.append("    FramePadding=");
	str += JsonHelper::imVec2ToString(this->style.FramePadding);
	str.append(",\n");

	str.append("    FrameRounding=");
	str.append2(this->style.FrameRounding);
	str.append(",\n");

	str.append("    FrameBorderSize=");
	str.append2(this->style.FrameBorderSize);
	str.append(",\n");

	str.append("    ItemSpacing=");
	str += JsonHelper::imVec2ToString(this->style.ItemSpacing);
	str.append(",\n");

	str.append("    ItemInnerSpacing=");
	str += JsonHelper::imVec2ToString(this->style.ItemInnerSpacing);
	str.append(",\n");

	str.append("    TouchExtraPadding=");
	str += JsonHelper::imVec2ToString(this->style.TouchExtraPadding);
	str.append(",\n");

	str.append("    IndentSpacing=");
	str.append2(this->style.IndentSpacing);
	str.append(",\n");

	str.append("    ColumnsMinSpacing=");
	str.append2(this->style.ColumnsMinSpacing);
	str.append(",\n");

	str.append("    ScrollbarSize=");
	str.append2(this->style.ScrollbarSize);
	str.append(",\n");

	str.append("    ScrollbarRounding=");
	str.append2(this->style.ScrollbarRounding);
	str.append(",\n");

	str.append("    GrabMinSize=");
	str.append2(this->style.GrabMinSize);
	str.append(",\n");

	str.append("    GrabRounding=");
	str.append2(this->style.GrabRounding);
	str.append(",\n");

	str.append("    TabRounding=");
	str.append2(this->style.TabRounding);
	str.append(",\n");

	str.append("    TabBorderSize=");
	str.append2(this->style.TabBorderSize);
	str.append(",\n");

	str.append("    ColorButtonPosition=");
	str += JsonHelper::imGuiDirToString(this->style.ColorButtonPosition);
	str.append(",\n");

	str.append("    ButtonTextAlign=");
	str += JsonHelper::imVec2ToString(this->style.ButtonTextAlign);
	str.append(",\n");

	str.append("    SelectableTextAlign=");
	str += JsonHelper::imVec2ToString(this->style.SelectableTextAlign);
	str.append(",\n");

	str.append("    DisplayWindowPadding=");
	str += JsonHelper::imVec2ToString(this->style.DisplayWindowPadding);
	str.append(",\n");

	str.append("    DisplaySafeAreaPadding=");
	str += JsonHelper::imVec2ToString(this->style.DisplaySafeAreaPadding);
	str.append(",\n");

	str.append("    MouseCursorScale=");
	str.append2(this->style.MouseCursorScale);
	str.append(",\n");

	str.append("    AntiAliasedLines=");
	if (this->style.AntiAliasedLines) {
		str.append("true");
	}
	else {
		str.append("false");
	}
	str.append(",\n");

	str.append("    AntiAliasedFill=");
	if (this->style.AntiAliasedFill) {
		str.append("true");
	}
	else {
		str.append("false");
	}
	str.append(",\n");

	str.append("    CurveTessellationTol=");
	str.append2(this->style.CurveTessellationTol);
	str.append(",\n");

	str.append("    colors={\n");
	for (auto& [name, colorEnum] : colorsNames) {
		str.append("        ");
		str += name;
		str.append("=");
		str += JsonHelper::imVec4ToString(this->style.Colors[colorEnum]);
		str.append(",\n");
	}
	str.append("\n    }");
	str.append("\n}");

	return str;
}


UString ImGuiStyleVerso::toStringDebug() const
{
	UString str("ImGuiStyleVerso(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso

// dear imgui: Platform Backend for Verso
// This needs to be used along with a Renderer (e.g. Verso, DirectX11, OpenGL3, Vulkan..)
// based on: Platform Backend for SDL2
//
// Implemented features: ???
//  [X] Platform: Clipboard support.
//  [X] Platform: Mouse support. Can discriminate Mouse/TouchScreen.
//  [X] Platform: Keyboard support. Since 1.87 we are using the io.AddKeyEvent() function. Pass ImGuiKey values to all key functions e.g. ImGui::IsKeyPressed(ImGuiKey_Space). [Legacy SDL_SCANCODE_* values will also be supported unless IMGUI_DISABLE_OBSOLETE_KEYIO is set]
//  [X] Platform: Gamepad support. Enabled with 'io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad'.
//  [X] Platform: Mouse cursor shape and visibility. Disable with 'io.ConfigFlags |= ImGuiConfigFlags_NoMouseCursorChange'.
//  [X] Platform: Basic IME support. App needs to call 'SDL_SetHint(SDL_HINT_IME_SHOW_UI, "1");' before SDL_CreateWindow()!.

#include "imgui.h"
#ifndef IMGUI_DISABLE
#include <Verso/Gui/imgui/imgui_impl_verso.hpp>

// Clang warnings with -Weverything
#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wimplicit-int-float-conversion"  // warning: implicit conversion from 'xxx' to 'float' may lose precision
#endif

// Verso
#include <Verso/Display/IWindowOpengl.hpp>
#include <Verso/Input/Event.hpp>
#include <Verso/Time/FrameTimestamp.hpp>
#include <Verso/Input/InputManager.hpp>

namespace Verso {


// Verso Data
struct ImGui_ImplVerso_Data
{
	IWindowOpengl*          Window;
	Uint64                  Time;
	UString                 ClipboardTextData;

	// Mouse handling
	Uint32                  MouseWindowID;
	int                     MouseButtonsDown;
	SystemCursorType        MouseCursors[ImGuiMouseCursor_COUNT];
	SystemCursorType        MouseLastCursor;
	int                     MouseLastLeaveFrame;
	bool                    MouseCanUseGlobalState;

	// TODO: Gamepad handling
	//ImVector<SDL_GameController*> Gamepads;
	//ImGui_ImplVerso_GamepadMode    GamepadMode;
	//bool                          WantUpdateGamepadsList;

	ImGui_ImplVerso_Data()   { memset(reinterpret_cast<void*>(this), 0, sizeof(*this)); }
};

// Backend data stored in io.BackendPlatformUserData to allow support for multiple Dear ImGui contexts
// It is STRONGLY preferred that you use docking branch with multi-viewports (== single Dear ImGui context + multiple windows) instead of multiple Dear ImGui contexts.
// FIXME: multi-context support is not well tested and probably dysfunctional in this backend.
// FIXME: some shared resources (mouse cursor shape, gamepad) are mishandled when using multi-context.
static ImGui_ImplVerso_Data* ImGui_ImplVerso_GetBackendData()
{
	return ImGui::GetCurrentContext() ? reinterpret_cast<ImGui_ImplVerso_Data*>(ImGui::GetIO().BackendPlatformUserData) : nullptr;
}

// Functions
static const char* ImGui_ImplVerso_GetClipboardText(void*)
{
    ImGui_ImplVerso_Data* bd = ImGui_ImplVerso_GetBackendData();
	bd->ClipboardTextData = bd->Window->getClipboardText().c_str();
	return bd->ClipboardTextData.c_str();
}

static void ImGui_ImplVerso_SetClipboardText(void*, const char* text)
{
	const ImGui_ImplVerso_Data* bd = ImGui_ImplVerso_GetBackendData();
	bd->Window->setClipboardText(text);
}

// Note: native IME will only display if user calls SDL_SetHint(SDL_HINT_IME_SHOW_UI, "1") _before_ SDL_CreateWindow().
static void ImGui_ImplVerso_SetPlatformImeData(ImGuiViewport*, ImGuiPlatformImeData* data)
{
	if (data->WantVisible)
	{
		const ImGui_ImplVerso_Data* bd = ImGui_ImplVerso_GetBackendData();
		bd->Window->setTextInputRect(
			Recti(
				(int)data->InputPos.x, (int)data->InputPos.y,
				1, (int)data->InputLineHeight));
	}
}

static ImGuiKey ImGui_ImplVerso_KeycodeToImGuiKey(KeyScancode keyScancode)
{
	switch (keyScancode)
	{
		case KeyScancode::Tab: return ImGuiKey_Tab;
		case KeyScancode::Left: return ImGuiKey_LeftArrow;
		case KeyScancode::Right: return ImGuiKey_RightArrow;
		case KeyScancode::Up: return ImGuiKey_UpArrow;
		case KeyScancode::Down: return ImGuiKey_DownArrow;
		case KeyScancode::PageUp: return ImGuiKey_PageUp;
		case KeyScancode::PageDown: return ImGuiKey_PageDown;
		case KeyScancode::Home: return ImGuiKey_Home;
		case KeyScancode::End: return ImGuiKey_End;
		case KeyScancode::Insert: return ImGuiKey_Insert;
		case KeyScancode::Del: return ImGuiKey_Delete;
		case KeyScancode::Backspace: return ImGuiKey_Backspace;
		case KeyScancode::Space: return ImGuiKey_Space;
		case KeyScancode::Return: return ImGuiKey_Enter;
		case KeyScancode::Escape: return ImGuiKey_Escape;
		case KeyScancode::Apostrophe: return ImGuiKey_Apostrophe;
		case KeyScancode::Comma: return ImGuiKey_Comma;
		case KeyScancode::Minus: return ImGuiKey_Minus;
		case KeyScancode::Period: return ImGuiKey_Period;
		case KeyScancode::Slash: return ImGuiKey_Slash;
		case KeyScancode::Semicolon: return ImGuiKey_Semicolon;
		case KeyScancode::Equals: return ImGuiKey_Equal;
		case KeyScancode::LeftBracket: return ImGuiKey_LeftBracket;
		case KeyScancode::Backslash: return ImGuiKey_Backslash;
		case KeyScancode::RightBracket: return ImGuiKey_RightBracket;
		case KeyScancode::Grave: return ImGuiKey_GraveAccent;
		case KeyScancode::CapsLock: return ImGuiKey_CapsLock;
		case KeyScancode::ScrollLock: return ImGuiKey_ScrollLock;
		case KeyScancode::NumLockClear: return ImGuiKey_NumLock;
		case KeyScancode::PrintScreen: return ImGuiKey_PrintScreen;
		case KeyScancode::Pause: return ImGuiKey_Pause;
		case KeyScancode::Kp0: return ImGuiKey_Keypad0;
		case KeyScancode::Kp1: return ImGuiKey_Keypad1;
		case KeyScancode::Kp2: return ImGuiKey_Keypad2;
		case KeyScancode::Kp3: return ImGuiKey_Keypad3;
		case KeyScancode::Kp4: return ImGuiKey_Keypad4;
		case KeyScancode::Kp5: return ImGuiKey_Keypad5;
		case KeyScancode::Kp6: return ImGuiKey_Keypad6;
		case KeyScancode::Kp7: return ImGuiKey_Keypad7;
		case KeyScancode::Kp8: return ImGuiKey_Keypad8;
		case KeyScancode::Kp9: return ImGuiKey_Keypad9;
		case KeyScancode::KpPeriod: return ImGuiKey_KeypadDecimal;
		case KeyScancode::KpDivide: return ImGuiKey_KeypadDivide;
		case KeyScancode::KpMultiply: return ImGuiKey_KeypadMultiply;
		case KeyScancode::KpMinus: return ImGuiKey_KeypadSubtract;
		case KeyScancode::KpPlus: return ImGuiKey_KeypadAdd;
		case KeyScancode::KpEnter: return ImGuiKey_KeypadEnter;
		case KeyScancode::KpEquals: return ImGuiKey_KeypadEqual;
		case KeyScancode::LCtrl: return ImGuiKey_LeftCtrl;
		case KeyScancode::LShift: return ImGuiKey_LeftShift;
		case KeyScancode::LAlt: return ImGuiKey_LeftAlt;
		case KeyScancode::LGui: return ImGuiKey_LeftSuper;
		case KeyScancode::RCtrl: return ImGuiKey_RightCtrl;
		case KeyScancode::RShift: return ImGuiKey_RightShift;
		case KeyScancode::RAlt: return ImGuiKey_RightAlt;
		case KeyScancode::RGui: return ImGuiKey_RightSuper;
		case KeyScancode::Application: return ImGuiKey_Menu;
		case KeyScancode::N0: return ImGuiKey_0;
		case KeyScancode::N1: return ImGuiKey_1;
		case KeyScancode::N2: return ImGuiKey_2;
		case KeyScancode::N3: return ImGuiKey_3;
		case KeyScancode::N4: return ImGuiKey_4;
		case KeyScancode::N5: return ImGuiKey_5;
		case KeyScancode::N6: return ImGuiKey_6;
		case KeyScancode::N7: return ImGuiKey_7;
		case KeyScancode::N8: return ImGuiKey_8;
		case KeyScancode::N9: return ImGuiKey_9;
		case KeyScancode::A: return ImGuiKey_A;
		case KeyScancode::B: return ImGuiKey_B;
		case KeyScancode::C: return ImGuiKey_C;
		case KeyScancode::D: return ImGuiKey_D;
		case KeyScancode::E: return ImGuiKey_E;
		case KeyScancode::F: return ImGuiKey_F;
		case KeyScancode::G: return ImGuiKey_G;
		case KeyScancode::H: return ImGuiKey_H;
		case KeyScancode::I: return ImGuiKey_I;
		case KeyScancode::J: return ImGuiKey_J;
		case KeyScancode::K: return ImGuiKey_K;
		case KeyScancode::L: return ImGuiKey_L;
		case KeyScancode::M: return ImGuiKey_M;
		case KeyScancode::N: return ImGuiKey_N;
		case KeyScancode::O: return ImGuiKey_O;
		case KeyScancode::P: return ImGuiKey_P;
		case KeyScancode::Q: return ImGuiKey_Q;
		case KeyScancode::R: return ImGuiKey_R;
		case KeyScancode::S: return ImGuiKey_S;
		case KeyScancode::T: return ImGuiKey_T;
		case KeyScancode::U: return ImGuiKey_U;
		case KeyScancode::V: return ImGuiKey_V;
		case KeyScancode::W: return ImGuiKey_W;
		case KeyScancode::X: return ImGuiKey_X;
		case KeyScancode::Y: return ImGuiKey_Y;
		case KeyScancode::Z: return ImGuiKey_Z;
		case KeyScancode::F1: return ImGuiKey_F1;
		case KeyScancode::F2: return ImGuiKey_F2;
		case KeyScancode::F3: return ImGuiKey_F3;
		case KeyScancode::F4: return ImGuiKey_F4;
		case KeyScancode::F5: return ImGuiKey_F5;
		case KeyScancode::F6: return ImGuiKey_F6;
		case KeyScancode::F7: return ImGuiKey_F7;
		case KeyScancode::F8: return ImGuiKey_F8;
		case KeyScancode::F9: return ImGuiKey_F9;
		case KeyScancode::F10: return ImGuiKey_F10;
		case KeyScancode::F11: return ImGuiKey_F11;
		case KeyScancode::F12: return ImGuiKey_F12;
		case KeyScancode::F13: return ImGuiKey_F13;
		case KeyScancode::F14: return ImGuiKey_F14;
		case KeyScancode::F15: return ImGuiKey_F15;
		case KeyScancode::F16: return ImGuiKey_F16;
		case KeyScancode::F17: return ImGuiKey_F17;
		case KeyScancode::F18: return ImGuiKey_F18;
		case KeyScancode::F19: return ImGuiKey_F19;
		case KeyScancode::F20: return ImGuiKey_F20;
		case KeyScancode::F21: return ImGuiKey_F21;
		case KeyScancode::F22: return ImGuiKey_F22;
		case KeyScancode::F23: return ImGuiKey_F23;
		case KeyScancode::F24: return ImGuiKey_F24;
		case KeyScancode::AcBack: return ImGuiKey_AppBack;
		case KeyScancode::AcForward: return ImGuiKey_AppForward;
	}
	return ImGuiKey_None;
}

static void ImGui_ImplVerso_UpdateKeyModifiers(Keymodifiers key_mods)
{
	ImGuiIO& io = ImGui::GetIO();
	io.AddKeyEvent(ImGuiMod_Ctrl, keyModifiersMatchRules(key_mods, static_cast<Keymodifiers>(Keymodifier::AnyCtrl)) != 0);
	io.AddKeyEvent(ImGuiMod_Shift, keyModifiersMatchRules(key_mods, static_cast<Keymodifiers>(Keymodifier::AnyShift)) != 0);
	io.AddKeyEvent(ImGuiMod_Alt, keyModifiersMatchRules(key_mods, static_cast<Keymodifiers>(Keymodifier::AnyAlt)) != 0);
	io.AddKeyEvent(ImGuiMod_Super, keyModifiersMatchRules(key_mods, static_cast<Keymodifiers>(Keymodifier::AnyGui)) != 0);
}

// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application, or clear/overwrite your copy of the mouse data.
// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application, or clear/overwrite your copy of the keyboard data.
// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
// If you have multiple SDL events and some of them are not meant to be used by dear imgui, you may need to filter events based on their windowID field.
bool ImGui_ImplVerso_ProcessEvent(const Event& event)
{
	ImGui_ImplVerso_Data* bd = ImGui_ImplVerso_GetBackendData();
	IM_ASSERT(bd != nullptr && "Context or backend not initialized! Did you call ImGui_ImplVerso_Init()?");
	ImGuiIO& io = ImGui::GetIO();

	switch (event.type)
	{
		case EventType::Mouse: {
			const MouseEvent& mouse = event.mouse;
			switch (mouse.type)
			{
				case MouseEventType::Motion:
				{
					ImVec2 mouse_pos(static_cast<float>(mouse.x), static_cast<float>(mouse.y));
					// TODO: touch events: io.AddMouseSourceEvent(event->motion.which == SDL_TOUCH_MOUSEID ? ImGuiMouseSource_TouchScreen : ImGuiMouseSource_Mouse);
					io.AddMouseSourceEvent(ImGuiMouseSource_Mouse); // TODO: touch events
					io.AddMousePosEvent(mouse_pos.x, mouse_pos.y);
					return true;
				}
				case MouseEventType::Wheel:
				{
					float wheel_x = -mouse.wheelMotionPreciseX;
					float wheel_y = mouse.wheelMotionPreciseY;
					// TODO: touch events: io.AddMouseSourceEvent(event->wheel.which == SDL_TOUCH_MOUSEID ? ImGuiMouseSource_TouchScreen : ImGuiMouseSource_Mouse);
					io.AddMouseSourceEvent(ImGuiMouseSource_Mouse); // TODO: touch events
					io.AddMouseWheelEvent(wheel_x, wheel_y);
					return true;
				}
				case MouseEventType::Button:
				{
					int mouse_button = -1;
					if (mouse.changedButton == MouseButton::Left) { mouse_button = 0; }
					if (mouse.changedButton == MouseButton::Right) { mouse_button = 1; }
					if (mouse.changedButton == MouseButton::Middle) { mouse_button = 2; }
					if (mouse.changedButton == MouseButton::X1) { mouse_button = 3; }
					if (mouse.changedButton == MouseButton::X2) { mouse_button = 4; }
					if (mouse_button == -1)
						break;
					// TODO: touch events: io.AddMouseSourceEvent(event->button.which == SDL_TOUCH_MOUSEID ? ImGuiMouseSource_TouchScreen : ImGuiMouseSource_Mouse);
					io.AddMouseSourceEvent(ImGuiMouseSource_Mouse); // TODO: touch events
					io.AddMouseButtonEvent(mouse_button, (mouse.state == ButtonState::Pressed));
					bd->MouseButtonsDown = (mouse.state == ButtonState::Pressed) ? (bd->MouseButtonsDown | (1 << mouse_button)) : (bd->MouseButtonsDown & ~(1 << mouse_button));
					return true;
				}
			}
		}

		case EventType::Keyboard:
		{
			const KeyboardEvent& keyboard = event.keyboard;
			switch (keyboard.type) {
				case KeyboardEventType::TextInput:
				{
					io.AddInputCharactersUTF8(keyboard.text.c_str());
					return true;
				}
				case KeyboardEventType::Key:
				{
					ImGui_ImplVerso_UpdateKeyModifiers(keyboard.key.keymodifiers);
					ImGuiKey key = ImGui_ImplVerso_KeycodeToImGuiKey(keyboard.key.scancode);
					io.AddKeyEvent(key, (keyboard.state == ButtonState::Pressed));
					io.SetKeyEventNativeData(key, static_cast<int>(keyboard.key.keyCode), static_cast<int>(keyboard.key.scancode), static_cast<int>(keyboard.key.scancode)); // To support legacy indexing (<1.87 user code). Legacy backend uses SDLK_*** as indices to IsKeyXXX() functions.
					return true;
				}
			}
		}

		// TODO: Window event support
		/*case EventType::Window:
		{
			// - When capturing mouse, SDL will send a bunch of conflicting LEAVE/ENTER event on every mouse move, but the final ENTER tends to be right.
			// - However we won't get a correct LEAVE event for a captured window.
			// - In some cases, when detaching a window from main viewport SDL may send SDL_WINDOWEVENT_ENTER one frame too late,
			//   causing SDL_WINDOWEVENT_LEAVE on previous frame to interrupt drag operation by clear mouse position. This is why
			//   we delay process the SDL_WINDOWEVENT_LEAVE events by one frame. See issue #5012 for details.
			Uint8 window_event = event->window.event;
			if (window_event == SDL_WINDOWEVENT_ENTER)
			{
				bd->MouseWindowID = event->window.windowID;
				bd->MouseLastLeaveFrame = 0;
			}
			if (window_event == SDL_WINDOWEVENT_LEAVE)
				bd->MouseLastLeaveFrame = ImGui::GetFrameCount() + 1;
			if (window_event == SDL_WINDOWEVENT_FOCUS_GAINED)
				io.AddFocusEvent(true);
			else if (event->window.event == SDL_WINDOWEVENT_FOCUS_LOST)
				io.AddFocusEvent(false);
			return true;
		}*/

		// TODO: gamepad support
		/*case SDL_CONTROLLERDEVICEADDED:
		case SDL_CONTROLLERDEVICEREMOVED:
		{
			bd->WantUpdateGamepadsList = true;
			return true;
		}*/
	}
	return false;
}

static bool ImGui_ImplVerso_Init(IWindowOpengl& window)
{
	ImGuiIO& io = ImGui::GetIO();
	IMGUI_CHECKVERSION();
	IM_ASSERT(io.BackendPlatformUserData == nullptr && "Already initialized a platform backend!");

	// Check and store if we are on a SDL backend that supports global mouse position
	// ("wayland" and "rpi" don't support it, but we chose to use a white-list instead of a black-list)
	bool mouse_can_use_global_state = false;
	UString sdl_backend = window.getCurrentVideoDriver();
	const char* global_mouse_whitelist[] = { "windows", "cocoa", "x11", "DIVE", "VMAN" };
	for (int n = 0; n < IM_ARRAYSIZE(global_mouse_whitelist); n++)
		if (sdl_backend.equalsIgnoreCase(global_mouse_whitelist[n]))
			mouse_can_use_global_state = true;

	// Setup backend capabilities flags
	ImGui_ImplVerso_Data* bd = IM_NEW(ImGui_ImplVerso_Data)();
	io.BackendPlatformUserData = (void*)bd;
	io.BackendPlatformName = "imgui_impl_verso";
	io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;       // We can honor GetMouseCursor() values (optional)
	io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;        // We can honor io.WantSetMousePos requests (optional, rarely used)

	bd->Window = &window;
	bd->MouseCanUseGlobalState = mouse_can_use_global_state;

	io.SetClipboardTextFn = ImGui_ImplVerso_SetClipboardText;
	io.GetClipboardTextFn = ImGui_ImplVerso_GetClipboardText;
	io.ClipboardUserData = nullptr;
	io.SetPlatformImeDataFn = ImGui_ImplVerso_SetPlatformImeData;

	// TODO: Gamepad handling
	//bd->GamepadMode = ImGui_ImplVerso_GamepadMode_AutoFirst;
	//bd->WantUpdateGamepadsList = true;

	// Load mouse cursors
	SystemCursorType temp;
	bd->MouseCursors[ImGuiMouseCursor_Arrow] = temp = SystemCursorType::Arrow; window.createSystemCursor(temp);
	bd->MouseCursors[ImGuiMouseCursor_TextInput] = temp = SystemCursorType::IBeam; window.createSystemCursor(temp);
	bd->MouseCursors[ImGuiMouseCursor_ResizeAll] = temp = SystemCursorType::SizeAll; window.createSystemCursor(temp);
	bd->MouseCursors[ImGuiMouseCursor_ResizeNS] = temp = SystemCursorType::SizeNs; window.createSystemCursor(temp);
	bd->MouseCursors[ImGuiMouseCursor_ResizeEW] = temp = SystemCursorType::SizeWe; window.createSystemCursor(temp);
	bd->MouseCursors[ImGuiMouseCursor_ResizeNESW] = temp = SystemCursorType::SizeNeSw; window.createSystemCursor(temp);
	bd->MouseCursors[ImGuiMouseCursor_ResizeNWSE] = temp = SystemCursorType::SizeNwSe; window.createSystemCursor(temp);
	bd->MouseCursors[ImGuiMouseCursor_Hand] = temp = SystemCursorType::Hand; window.createSystemCursor(temp);
	bd->MouseCursors[ImGuiMouseCursor_NotAllowed] = temp = SystemCursorType::No; window.createSystemCursor(temp);

	// Set platform dependent data in viewport
	// Our mouse update function expect PlatformHandle to be filled for the main viewport
	ImGuiViewport* main_viewport = ImGui::GetMainViewport();
	main_viewport->PlatformHandleRaw = window.getNativeWindowHandle();

	// TODO: mouse focus clickthrough in IWindow
	// From 2.0.5: Set SDL hint to receive mouse click events on window focus, otherwise SDL doesn't emit the event.
	// Without this, when clicking to gain focus, our widgets wouldn't activate even though they showed as hovered.
	// (This is unfortunately a global SDL setting, so enabling it might have a side-effect on your application.
	// It is unlikely to make a difference, but if your app absolutely needs to ignore the initial on-focus click:
	// you can ignore SDL_MOUSEBUTTONDOWN events coming right after a SDL_WINDOWEVENT_FOCUS_GAINED)
#ifdef SDL_HINT_MOUSE_FOCUS_CLICKTHROUGH
	SDL_SetHint(SDL_HINT_MOUSE_FOCUS_CLICKTHROUGH, "1");
#endif

	// From 2.0.18: Enable native IME.
	// IMPORTANT: This is used at the time of SDL_CreateWindow() so this will only affects secondary windows, if any.
	// For the main window to be affected, your application needs to call this manually before calling SDL_CreateWindow().
	// NOTE: Native IME has been enabled in WindowOpengl
//#ifdef SDL_HINT_IME_SHOW_UI
//	SDL_SetHint(SDL_HINT_IME_SHOW_UI, "1");
//#endif

	// TODO: auto capture in IWindow
	// From 2.0.22: Disable auto-capture, this is preventing drag and drop across multiple windows (see #5710)
#ifdef SDL_HINT_MOUSE_AUTO_CAPTURE
	SDL_SetHint(SDL_HINT_MOUSE_AUTO_CAPTURE, "0");
#endif

	return true;
}


bool ImGui_ImplVerso_InitForOpenGL(IWindowOpengl& window, void* sdl_gl_context)
{
	IM_UNUSED(sdl_gl_context); // Viewport branch will need this.
	return ImGui_ImplVerso_Init(window);
}


static void ImGui_ImplVerso_CloseGamepads();


void ImGui_ImplVerso_Shutdown()
{
	ImGui_ImplVerso_Data* bd = ImGui_ImplVerso_GetBackendData();
	IM_ASSERT(bd != nullptr && "No platform backend to shutdown, or already shutdown?");
	ImGuiIO& io = ImGui::GetIO();

	// NOTE: no need to free UString
	//if (bd->ClipboardTextData)
	//	SDL_free(bd->ClipboardTextData);
	for (ImGuiMouseCursor cursor_n = 0; cursor_n < ImGuiMouseCursor_COUNT; cursor_n++)
		bd->Window->destroySystemCursor(bd->MouseCursors[cursor_n]);
	//ImGui_ImplVerso_CloseGamepads(); // TODO: gamepad support

	io.BackendPlatformName = nullptr;
	io.BackendPlatformUserData = nullptr;
	io.BackendFlags &= ~(ImGuiBackendFlags_HasMouseCursors | ImGuiBackendFlags_HasSetMousePos /*| ImGuiBackendFlags_HasGamepad*/); // TODO:  gamepad support
	IM_DELETE(bd);
}

static void ImGui_ImplVerso_UpdateMouseData()
{
	ImGui_ImplVerso_Data* bd = ImGui_ImplVerso_GetBackendData();
	ImGuiIO& io = ImGui::GetIO();

	// TODO: Capture & global mouse
	// We forward mouse input when hovered or captured (via SDL_MOUSEMOTION) or when focused (below)
	// SDL_CaptureMouse() let the OS know e.g. that our imgui drag outside the SDL window boundaries shouldn't e.g. trigger other operations outside
	SDL_CaptureMouse((bd->MouseButtonsDown != 0) ? SDL_TRUE : SDL_FALSE);
	SDL_Window* focused_window = SDL_GetKeyboardFocus();
	const bool is_app_focused = (reinterpret_cast<SDL_Window*>(bd->Window->getNativeWindowHandle()) == focused_window);

	if (is_app_focused)
	{
		// (Optional) Set OS mouse position from Dear ImGui if requested (rarely used, only when ImGuiConfigFlags_NavEnableSetMousePos is enabled by user)
		if (io.WantSetMousePos)
			bd->Window->warpMouseInWindow(static_cast<int>(io.MousePos.x), static_cast<int>(io.MousePos.y));

		// (Optional) Fallback to provide mouse position when focused (SDL_MOUSEMOTION already provides this when hovered or captured)
        if (bd->MouseCanUseGlobalState && bd->MouseButtonsDown == 0)
		{
			MouseState mouseState = bd->Window->getMouseState();
			Vector2i windowPos = bd->Window->getPositioni();
			io.AddMousePosEvent((float)(mouseState.desktopX - windowPos.x), (float)(mouseState.desktopY - windowPos.y));
		}
	}
}

static void ImGui_ImplVerso_UpdateMouseCursor()
{
	ImGuiIO& io = ImGui::GetIO();
	if (io.ConfigFlags & ImGuiConfigFlags_NoMouseCursorChange)
		return;
	ImGui_ImplVerso_Data* bd = ImGui_ImplVerso_GetBackendData();

	ImGuiMouseCursor imgui_cursor = ImGui::GetMouseCursor();
	if (io.MouseDrawCursor || imgui_cursor == ImGuiMouseCursor_None)
	{
		// Hide OS mouse cursor if imgui is drawing it or if it wants no cursor
		SDL_ShowCursor(SDL_FALSE);
	}
	else
	{
		// Show OS mouse cursor
		SystemCursorType expected_cursor = bd->MouseCursors[imgui_cursor] != SystemCursorType::Unset ? bd->MouseCursors[imgui_cursor] : bd->MouseCursors[ImGuiMouseCursor_Arrow];
		if (bd->MouseLastCursor != expected_cursor)
		{
			bd->Window->setSystemCursor(expected_cursor); // SDL function doesn't have an early out (see #6113)
			bd->MouseLastCursor = expected_cursor;
		}
		SDL_ShowCursor(SDL_TRUE);
	}
}

// TODO: gamepad support
/*static void ImGui_ImplVerso_CloseGamepads()
{
	ImGui_ImplSDL2_Data* bd = ImGui_ImplSDL2_GetBackendData();
	if (bd->GamepadMode != ImGui_ImplSDL2_GamepadMode_Manual)
		for (SDL_GameController* gamepad : bd->Gamepads)
			SDL_GameControllerClose(gamepad);
	bd->Gamepads.resize(0);
}

void ImGui_ImplVerso_SetGamepadMode(ImGui_ImplVerso_GamepadMode mode, struct _SDL_GameController** manual_gamepads_array, int manual_gamepads_count)
{
	ImGui_ImplSDL2_Data* bd = ImGui_ImplSDL2_GetBackendData();
	ImGui_ImplSDL2_CloseGamepads();
	if (mode == ImGui_ImplSDL2_GamepadMode_Manual)
	{
		IM_ASSERT(manual_gamepads_array != nullptr && manual_gamepads_count > 0);
		for (int n = 0; n < manual_gamepads_count; n++)
			bd->Gamepads.push_back(manual_gamepads_array[n]);
	}
	else
	{
		IM_ASSERT(manual_gamepads_array == nullptr && manual_gamepads_count <= 0);
		bd->WantUpdateGamepadsList = true;
	}
	bd->GamepadMode = mode;
}

static void ImGui_ImplVerso_UpdateGamepadButton(ImGui_ImplVerso_Data* bd, ImGuiIO& io, ImGuiKey key, SDL_GameControllerButton button_no)
{
	bool merged_value = false;
	for (SDL_GameController* gamepad : bd->Gamepads)
		merged_value |= SDL_GameControllerGetButton(gamepad, button_no) != 0;
	io.AddKeyEvent(key, merged_value);
}

static inline float Saturate(float v) { return v < 0.0f ? 0.0f : v  > 1.0f ? 1.0f : v; }
static void ImGui_ImplVerso_UpdateGamepadAnalog(ImGui_ImplVerso_Data* bd, ImGuiIO& io, ImGuiKey key, SDL_GameControllerAxis axis_no, float v0, float v1)
{
	float merged_value = 0.0f;
	for (SDL_GameController* gamepad : bd->Gamepads)
	{
		float vn = Saturate((float)(SDL_GameControllerGetAxis(gamepad, axis_no) - v0) / (float)(v1 - v0));
		if (merged_value < vn)
			merged_value = vn;
	}
	io.AddKeyAnalogEvent(key, merged_value > 0.1f, merged_value);
}

static void ImGui_ImplVerso_UpdateGamepads()
{
    ImGui_ImplVerso_Data* bd = ImGui_ImplVerso_GetBackendData();
    ImGuiIO& io = ImGui::GetIO();

    // Update list of controller(s) to use
    if (bd->WantUpdateGamepadsList && bd->GamepadMode != ImGui_ImplVerso_GamepadMode_Manual)
    {
        ImGui_ImplVerso_CloseGamepads();
        int joystick_count = SDL_NumJoysticks();
        for (int n = 0; n < joystick_count; n++)
            if (SDL_IsGameController(n))
                if (SDL_GameController* gamepad = SDL_GameControllerOpen(n))
                {
                    bd->Gamepads.push_back(gamepad);
                    if (bd->GamepadMode == ImGui_ImplVerso_GamepadMode_AutoFirst)
                        break;
                }
        bd->WantUpdateGamepadsList = false;
    }

    // FIXME: Technically feeding gamepad shouldn't depend on this now that they are regular inputs.
    if ((io.ConfigFlags & ImGuiConfigFlags_NavEnableGamepad) == 0)
        return;
    io.BackendFlags &= ~ImGuiBackendFlags_HasGamepad;
    if (bd->Gamepads.Size == 0)
        return;
    io.BackendFlags |= ImGuiBackendFlags_HasGamepad;

    // Update gamepad inputs
    const int thumb_dead_zone = 8000; // SDL_gamecontroller.h suggests using this value.
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadStart,       SDL_CONTROLLER_BUTTON_START);
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadBack,        SDL_CONTROLLER_BUTTON_BACK);
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadFaceLeft,    SDL_CONTROLLER_BUTTON_X);              // Xbox X, PS Square
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadFaceRight,   SDL_CONTROLLER_BUTTON_B);              // Xbox B, PS Circle
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadFaceUp,      SDL_CONTROLLER_BUTTON_Y);              // Xbox Y, PS Triangle
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadFaceDown,    SDL_CONTROLLER_BUTTON_A);              // Xbox A, PS Cross
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadDpadLeft,    SDL_CONTROLLER_BUTTON_DPAD_LEFT);
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadDpadRight,   SDL_CONTROLLER_BUTTON_DPAD_RIGHT);
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadDpadUp,      SDL_CONTROLLER_BUTTON_DPAD_UP);
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadDpadDown,    SDL_CONTROLLER_BUTTON_DPAD_DOWN);
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadL1,          SDL_CONTROLLER_BUTTON_LEFTSHOULDER);
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadR1,          SDL_CONTROLLER_BUTTON_RIGHTSHOULDER);
    ImGui_ImplVerso_UpdateGamepadAnalog(bd, io, ImGuiKey_GamepadL2,          SDL_CONTROLLER_AXIS_TRIGGERLEFT,  0.0f, 32767);
    ImGui_ImplVerso_UpdateGamepadAnalog(bd, io, ImGuiKey_GamepadR2,          SDL_CONTROLLER_AXIS_TRIGGERRIGHT, 0.0f, 32767);
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadL3,          SDL_CONTROLLER_BUTTON_LEFTSTICK);
    ImGui_ImplVerso_UpdateGamepadButton(bd, io, ImGuiKey_GamepadR3,          SDL_CONTROLLER_BUTTON_RIGHTSTICK);
    ImGui_ImplVerso_UpdateGamepadAnalog(bd, io, ImGuiKey_GamepadLStickLeft,  SDL_CONTROLLER_AXIS_LEFTX,  -thumb_dead_zone, -32768);
    ImGui_ImplVerso_UpdateGamepadAnalog(bd, io, ImGuiKey_GamepadLStickRight, SDL_CONTROLLER_AXIS_LEFTX,  +thumb_dead_zone, +32767);
    ImGui_ImplVerso_UpdateGamepadAnalog(bd, io, ImGuiKey_GamepadLStickUp,    SDL_CONTROLLER_AXIS_LEFTY,  -thumb_dead_zone, -32768);
    ImGui_ImplVerso_UpdateGamepadAnalog(bd, io, ImGuiKey_GamepadLStickDown,  SDL_CONTROLLER_AXIS_LEFTY,  +thumb_dead_zone, +32767);
    ImGui_ImplVerso_UpdateGamepadAnalog(bd, io, ImGuiKey_GamepadRStickLeft,  SDL_CONTROLLER_AXIS_RIGHTX, -thumb_dead_zone, -32768);
    ImGui_ImplVerso_UpdateGamepadAnalog(bd, io, ImGuiKey_GamepadRStickRight, SDL_CONTROLLER_AXIS_RIGHTX, +thumb_dead_zone, +32767);
    ImGui_ImplVerso_UpdateGamepadAnalog(bd, io, ImGuiKey_GamepadRStickUp,    SDL_CONTROLLER_AXIS_RIGHTY, -thumb_dead_zone, -32768);
    ImGui_ImplVerso_UpdateGamepadAnalog(bd, io, ImGuiKey_GamepadRStickDown,  SDL_CONTROLLER_AXIS_RIGHTY, +thumb_dead_zone, +32767);
}*/

void ImGui_ImplVerso_NewFrame(IWindowOpengl& window, const FrameTimestamp& time, bool useRenderResolution)
{
	(void)window; (void)time; (void)useRenderResolution;

	ImGui_ImplVerso_Data* bd = ImGui_ImplVerso_GetBackendData();
	IM_ASSERT(bd != nullptr && "Context or backend not initialized! Did you call ImGui_ImplVerso_Init()?");
	ImGuiIO& io = ImGui::GetIO();

    // Setup display size (every frame to accommodate for window resizing)
    int w, h;
    int display_w, display_h;
	Vector2i windowSize = bd->Window->getWindowResolutioni();
	w = windowSize.x; h = windowSize.y;
	if (bd->Window->isMinimized())
		w = h = 0;
	Vector2i drawableResolution = bd->Window->getDrawableResolutioni();
	display_w = drawableResolution.x; display_h = drawableResolution.y;
	io.DisplaySize = ImVec2((float)w, (float)h);
	if (w > 0 && h > 0)
		io.DisplayFramebufferScale = ImVec2((float)display_w / w, (float)display_h / h);

	// Setup time step (we don't use SDL_GetTicks() because it is using millisecond resolution)
	// (Accept SDL_GetPerformanceCounter() not returning a monotonically increasing value. Happens in VMs and Emscripten, see #6189, #6114, #3644)
	static Uint64 frequency = SDL_GetPerformanceFrequency();
	Uint64 current_time = SDL_GetPerformanceCounter();
	if (current_time <= bd->Time)
		current_time = bd->Time + 1;
	io.DeltaTime = bd->Time > 0 ? (float)((double)(current_time - bd->Time) / frequency) : (float)(1.0f / 60.0f);
	bd->Time = current_time;

	if (bd->MouseLastLeaveFrame && bd->MouseLastLeaveFrame >= ImGui::GetFrameCount() && bd->MouseButtonsDown == 0)
	{
		bd->MouseWindowID = 0;
		bd->MouseLastLeaveFrame = 0;
		io.AddMousePosEvent(-FLT_MAX, -FLT_MAX);
	}

	ImGui_ImplVerso_UpdateMouseData();
	ImGui_ImplVerso_UpdateMouseCursor();

	// Update game controllers (if enabled and available)
	//ImGui_ImplVerso_UpdateGamepads(); // TODO: gamepad support
}

//-----------------------------------------------------------------------------


} // End namespace Verso


#if defined(__clang__)
#pragma clang diagnostic pop
#endif

#endif // #ifndef IMGUI_DISABLE

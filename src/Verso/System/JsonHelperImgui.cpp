#include <Verso/System/JsonHelperImgui.hpp>
#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui.h>
#include <imgui_internal.h>

namespace Verso {

namespace JsonHelper {


ImVec2 readImVec2(const JsonPath& parent, const UString& name, bool required, const ImVec2& defaultValue)
{
	const JSONArray& vectorArr = parent.readArray(name, required);
	if (vectorArr.size() <= 0) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \"" + parent.currentPath + "." + name + "\"!");
			VERSO_ILLEGALFORMAT("verso-imgui", error.c_str(), parent.fileName.c_str());
		}
	}
	if (vectorArr.size() != 2) {
		UString error("Invalid type of values in field \"" + parent.currentPath + "." + name + "\"! ImVec2 array must contain exactly 2 values.");
		VERSO_ILLEGALFORMAT("verso-imgui", error.c_str(), parent.fileName.c_str());
	}

	bool typeError = false;
	double x = 0.0f, y = 0.0f;
	if (vectorArr[0]->IsNumber()) {
		x = vectorArr[0]->AsNumber();
	}
	else {
		typeError = true;
	}

	if (vectorArr[1]->IsNumber()) {
		y = vectorArr[1]->AsNumber();
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \"" + parent.currentPath + "." + name + "\"! ImVec2 array must contain only number values.");
		VERSO_ILLEGALFORMAT("verso-imgui", error.c_str(), parent.fileName.c_str());
	}

	return ImVec2(static_cast<float>(x), static_cast<float>(y));
}


ImVec4 readImVec4(const JsonPath& parent, const UString& name, bool required, const ImVec4& defaultValue)
{
	const JSONArray& vectorArr = parent.readArray(name, required);
	if (vectorArr.size() <= 0) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \"" + parent.currentPath + "." + name + "\"!");
			VERSO_ILLEGALFORMAT("verso-imgui", error.c_str(), parent.fileName.c_str());
		}
	}
	if (vectorArr.size() != 4) {
		UString error("Invalid type of values in field \"" + parent.currentPath + "." + name + "\"! ImVec4 array must contain exactly 4 values.");
		VERSO_ILLEGALFORMAT("verso-imgui", error.c_str(), parent.fileName.c_str());
	}

	bool typeError = false;
	double x = 0.0f, y = 0.0f, z = 0.0f, k = 0.0f;
	if (vectorArr[0]->IsNumber()) {
		x = vectorArr[0]->AsNumber();
	}
	else {
		typeError = true;
	}

	if (vectorArr[1]->IsNumber()) {
		y = vectorArr[1]->AsNumber();
	}
	else {
		typeError = true;
	}

	if (vectorArr[2]->IsNumber()) {
		z = vectorArr[2]->AsNumber();
	}
	else {
		typeError = true;
	}

	if (vectorArr[3]->IsNumber()) {
		k = vectorArr[3]->AsNumber();
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \"" + parent.currentPath + "." + name + "\"! ImVec4 array must contain only number values.");
		VERSO_ILLEGALFORMAT("verso-imgui", error.c_str(), parent.fileName.c_str());
	}

	return ImVec4(static_cast<float>(x), static_cast<float>(y), static_cast<float>(z), static_cast<float>(k));
}


ImGuiDir readImGuiDir(const JsonPath& parent, const UString& name, bool required, const ImGuiDir& defaultValue)
{
	UString imGuiDirStr = parent.readString(name, required, "");
	if (!imGuiDirStr.isEmpty()) {
		ImGuiDir dir = stringToImGuiDir(imGuiDirStr);
		if (dir == ImGuiDir_COUNT) {
			UString error("Unknown value (" + imGuiDirStr + ") in field \"" + parent.currentPath + "." + name + R"("! Must be one of: "None", "Left", "Right", "Up", "Down".)");
			VERSO_ILLEGALFORMAT("verso-imgui", error.c_str(), parent.fileName.c_str());
		}
		return dir;
	}
	else if (required == true) {
		UString error("Cannot read required field \"" + parent.currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-imgui", error.c_str(), parent.fileName.c_str());
	}

	return defaultValue;
}


} // End namespace JsonHelper


} // End namespace Verso

